
Game Utilities: Message

This provides an API for setting, storing, and displaying game messages.

game_set_message($type, $message, $variables = array(), $recipients = array());
game_get_messages($oid, $from = 0, $count = NULL);

@TODO:
views integration
